using System;
using Microsoft.Extensions.Options;
using WebApplication1.Managers;
using Xunit;
using Xunit.Abstractions;

namespace TestProject1
{
    public class EncryptTest
    {
        private const string publicKey = "jXn2r5u8x/A?D(G-KaPdSgVkYp3s6v9y";

        private readonly ITestOutputHelper output;

        public EncryptTest(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Theory]
        [InlineData("12345")]
        public void EncryptAndDecrypt(string testString)
        {
            try
            {
                var encryptOption = Options.Create(new EncryptManagerOptions());
                encryptOption.Value.PublicKey = publicKey;
                encryptOption.Value.KeySize = 256;

                var newEncryptManager = new EncryptManager(encryptOption);

                var encrypted = newEncryptManager.AesEncrypt(testString);

                this.output.WriteLine($"Encrypted : {encrypted}");

                var decrypted = newEncryptManager.AesDecrypt(encrypted);

                this.output.WriteLine($"Decrypted : {decrypted}");

                Assert.Equal(testString, decrypted);
            }
            catch (Exception ex)
            {
                Assert.True(false, ex.Message + ex.StackTrace);
            }
        }

        [Theory]
        [InlineData("12345")]
        public void EncryptAndDecryptOutVi(string testString)
        {
            try
            {
                var encryptOption = Options.Create(new EncryptManagerOptions());
                encryptOption.Value.PublicKey = publicKey;
                encryptOption.Value.KeySize = 256;

                var newEncryptManager = new EncryptManager(encryptOption);

                byte[] vi = null;

                var encrypted = newEncryptManager.AesEncryptOutVi(testString, out vi);

                this.output.WriteLine($"Encrypted : {encrypted}");

                var decrypted = newEncryptManager.AesDecryptOutVi(encrypted, vi);

                this.output.WriteLine($"Decrypted : {decrypted}");

                Assert.Equal(testString, decrypted);
            }
            catch (Exception ex)
            {
                Assert.True(false, ex.Message + ex.StackTrace);
            }
        }
    }
}
