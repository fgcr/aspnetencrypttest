﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Microsoft.Extensions.Options;

namespace WebApplication1.Managers
{
    public class EncryptManager
    {
        private byte[] _publicKey;
        private int _keySize = 256;
        private int _blockSize = 128;
        private CipherMode _cipherMode = CipherMode.CBC;
        private PaddingMode _paddingMode = PaddingMode.PKCS7;

        public EncryptManager(IOptions<EncryptManagerOptions> options)
        {
            this._keySize = options.Value.KeySize;
            this._publicKey = new byte[]
            {
                0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
                0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16
            };
        }

        public byte[] AesEncrypt(string targetString)
        {
            byte[] encrypted = null;

            using (var ms = new MemoryStream())
            {
                using (var aes = new AesManaged())
                {
                    
                    aes.Key = this._publicKey;

                    byte[] iv = aes.IV;
                    ms.Write(iv, 0, iv.Length);

                    using (var cs = new CryptoStream(
                               ms, 
                               aes.CreateEncryptor(), 
                               CryptoStreamMode.Write))
                    {
                        using (var sw = new StreamWriter(cs))
                        {
                            sw.Write(targetString);
                        }
                    }

                    encrypted = ms.ToArray();
                }
            }

            return encrypted;
        }

        public string AesDecrypt(byte[] targetData)
        {
            string result = null;

            using (var ms = new MemoryStream(targetData))
            {
                using (var aes = new AesManaged())
                {
                    byte[] iv = new byte[aes.IV.Length];
                    int numBytesToRead = aes.IV.Length;
                    int numBytesRead = 0;
                    while (numBytesToRead > 0)
                    {
                        int n = ms.Read(iv, numBytesRead, numBytesToRead);
                        if (n == 0) break;

                        numBytesRead += n;
                        numBytesToRead -= n;
                    }

                    using (var cs = new CryptoStream(
                               ms,
                               aes.CreateDecryptor(this._publicKey, iv), 
                               CryptoStreamMode.Read))
                    {
                        using (var sr = new StreamReader(cs))
                        {
                            result = sr.ReadToEnd();
                        }
                    }
                }
            }

            return result;
        }

        public string AesEncryptOutVi(string targetString, out byte[] iv)
        {
            string result = null;
            byte[] encrypted = null;

            using (var aes = new AesManaged())
            {
                aes.Mode = _cipherMode;
                aes.Padding = _paddingMode;
                aes.KeySize = _keySize;
                aes.BlockSize = _blockSize;

                aes.Key = this._publicKey;
                iv = aes.IV;

                using (var encryptor = aes.CreateEncryptor(aes.Key, aes.IV))
                {
                    using (var ms = new MemoryStream())
                    {
                        using (var cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
                        {
                            using (var sw = new StreamWriter(cs))
                            {
                                sw.Write(targetString);
                            }
                        }

                        encrypted = ms.ToArray();
                    }

                    result = Encoding.UTF8.GetString(encrypted);
                }
            }

            return result;
        }

        public string AesDecryptOutVi(string targetString, byte[] iv)
        {
            string result = null;

            using (var aes = new AesManaged())
            {
                aes.Mode = _cipherMode;
                aes.Padding = _paddingMode;
                aes.KeySize = _keySize;
                aes.BlockSize = _blockSize;

                var dataArr = Encoding.UTF8.GetBytes(targetString);

                aes.Key = this._publicKey;
                aes.IV = iv;

                using (var decryptor = aes.CreateDecryptor(aes.Key, aes.IV))
                {
                    using (var ms = new MemoryStream(dataArr))
                    {
                        using (var cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Read))
                        {
                            using (var sr = new StreamReader(ms))
                            {
                                result = sr.ReadToEnd();
                            }
                        }
                    }
                }
            }

            return result;
        }
    }

    public class EncryptManagerOptions
    {
        public string PublicKey { get; set; }
        public int KeySize { get; set; }
    }
}
